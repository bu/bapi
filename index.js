var http = require("http"),
	query_string = require("querystring"),
	path = require("path"),
	sha1 = require("sha1");

var	config = require( path.join(__dirname, "..", "..", "config", "generalConfig") );

var api_instance_pool = {};

exports.getInstanceOf = function(api_set_name) {
	if(typeof api_set_name === "undefined") {
		api_set_name = config.api.default_set;
	}

	if(typeof api_instance_pool[api_set_name] === "undefined") {
		if(typeof config.api.sets[api_set_name] === "undefined") {
			throw new Error("Given API set name '" + api_set_name + "' not found in config file.");
		}

		var this_set = config.api.sets[api_set_name];

		api_instance_pool[api_set_name] = new bAPI(this_set.api_host, this_set.api_port, this_set.api_accessID, this_set.api_accessKey);
	}

	return api_instance_pool[api_set_name];
};

exports.PostRequest = function() {
	throw new Error("PostRequest direct access is removed. Access by get an API instance.");
};

var bAPI = function(api_host, api_port, api_accessID, api_accessKey) {
	var self = this;

	self.api_host = api_host;
	self.api_port = api_port;
	self.api_accessID = api_accessID;
	self.api_accessKey = api_accessKey;

	return self;
}

// helper method
bAPI.prototype._request = function(method, api_uri, data, callback) {
	var self = this;

	var query_dataString = query_string.stringify(data);
	
	var options = {
		host: self.api_host,
		port: self.api_port,
		path: api_uri,
		headers: {
			'AccessKeyID': self.api_accessID,
			'Signature': self._hmac(query_dataString),
			'Content-Type': 'application/x-www-form-urlencoded',  
			'Content-Length': query_dataString.length
		},
		method: method
	};

	var req = http.request(options, function(res) {
		res.setEncoding('utf8');

		res.on('data', function(chunk) {
			process.nextTick(function() {
				callback(null, chunk, res.statusCode);
			});
		});
		
	});

	req.on('error', function(e) {
		return callback(e);
	});

	// write data to request body
	req.write(query_dataString);
	req.end();
};

bAPI.prototype._hmac = function(dataString) {
	return sha1(dataString + this.api_accessKey);
};

// post
bAPI.prototype.PostRequest = function(api_uri, data, callback) {
	var query_data = (typeof data === "function") ? {} : data;
	var query_callback = (typeof data === "function") ? data : callback;

	return this._request("POST", api_uri, query_data, query_callback);
};

// put
bAPI.prototype.PutRequest = function(api_uri, data, callback) {
	return this._request("PUT", api_uri, data, callback);
}

bAPI.prototype.DeleteRequest = function(api_uri, callback) {
	return this._request("DELETE", api_uri, {}, callback);
};

bAPI.prototype.GetRequest = function(api_uri, data, callback) {
	var self = this, config = this;
	if(typeof data === "function") {
		var callback = data;
		var userdata = {};
	} else {
		var userdata = data;
	}

	userdata.AccessKeyID = config.api_accessID;

	var userdata_querystring = query_string.stringify(userdata);

	userdata.Signature = sha1(userdata_querystring + config.api_accessKey);

	var querystring = query_string.stringify(userdata);

	http.get("http://" + config.api_host + ":" + config.api_port + api_uri + "?" + querystring, function(res) {
		res.setEncoding('utf8');

		res.on('data', function(chunk) {
			process.nextTick(function() {
				callback(null, chunk, res.statusCode);
			});
		});
	}).on("error", function(e) {
		callback(e);
	});
};
